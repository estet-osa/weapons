PHP вместо САЙГИ :)
===================

В примере используется паттерн: `builder`. Не менее гармонично вписался `Trait` `Weapons`.

**1. Стрельба из пистолета:**

```
(new Pistol)
    ->intro()
    ->checkMagazine()
    ->reload()
    ->checkMagazine()
    ->bang()
    ->bang()
    ->bang()
    ->bang()
    ->bang()
    
Shoots a Pistol
    В обойме осталось 0 патронов
    RELOAD
    В обойме осталось 3 патронов
    BANG
    BANG
    BANG
    CLICK
    CLICK
```
**2. Стрельба из базуки:**
```
(new Bazooka)
    ->intro()
    ->checkMagazine()
    ->bang()
    ->reload()
    ->bang()
    ->bang();

Shoots a Bazooka
    В обойме осталось 0 патронов
    CLICK
    RELOAD
    BANG
    CLICK
```