<?php

interface WeaponInterface
{
    const ZERO_ROUNDS = 0;
}

/**
 * Using trait for all weapons as you will :)
 * Trait WeaponTrait
 */
trait WeaponTrait
{
    /** @var int $magazine */
    private $magazine = 0;
    /** @var int $counter */
    private $counter = 0;

    /**
     * Weapons constructor.
     */
    public function __construct()
    {
        /** @var int magazine */
        $this->magazine = self::MAGAZINE;
    }

    /**
     * @return WeaponTrait
     */
    public function intro(): self
    {
        echo '<h1>Shoots a ' . self::class . '</h1>';

        return $this;
    }

    /**
     * @return WeaponTrait
     */
    public function checkMagazine(): self
    {
        echo "В обойме осталось {$this->counter} патронов <br>";

        return $this;
    }

    /**
     * @return WeaponTrait
     */
    public function reload (): self
    {
        echo 'RELOAD <br>';

        /** @var int counter */
        $this->counter = $this->magazine;

        return $this;
    }

    /**
     * @return WeaponTrait
     */
    public function bang (): self
    {
        if ($this->counter !== self::ZERO_ROUNDS) {
            $this->counter--;
            echo 'BANG <br>';
        } else
            echo 'CLICK <br>';
        return $this;
    }
}

Class Pistol implements WeaponInterface
{
    const MAGAZINE = 3;
    use WeaponTrait;
}

Class Bazooka implements WeaponInterface
{
    const MAGAZINE = 1;
    use WeaponTrait;
}

(new Pistol)
    ->intro()
    ->checkMagazine()
    ->reload()
    ->checkMagazine()
    ->bang()
    ->bang()
    ->bang()
    ->bang()
    ->bang();

(new Bazooka)
    ->intro()
    ->checkMagazine()
    ->bang()
    ->reload()
    ->bang()
    ->bang();
